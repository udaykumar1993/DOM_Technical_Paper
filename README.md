# DOM
The [**Document Object Model**(*DOM*)](https://developer.mozilla.org/en-US/docs/Web/API/Document_Object_Model/Introduction) is the data representation of the objects that comprise the structure and content of a document on the web.

---

## What is the DOM?
The **Document Object Model** (*DOM) is a programming interface for web documents. It represents the page so that programs can change the document structure,  style, and content. The DOM represents the document as nodes and objects. That way, Programming languages can interact with the page.

___

##  How does it help?
<p style='text-align: justify;'>JavaScript doesn't do much more than allow us to‌‌ perform some calculations or work with basic strings.</p>

<p style='text-align: justify;'>So to make an HTML document more interactive and dynamic, the script‌‌ needs to be able to access the contents of the document and It also needs to know when the user is interacting with it.‌‌</p>

<p style='text-align: justify;'>It does this by communicating with the browser using the properties,  Methods, And events in the interface called the Document Object Model, or DOM.</p>

___

## [Accessing the DOM](https://developer.mozilla.org/en-US/docs/Web/API/Document_Object_Model/Introduction)
<p style='text-align: justify;'> We don't have to do anything special to begin using the DOM. We use the API directly in JavaScript from within what is called a script, a  program run by a browser.</p>

<p style='text-align: justify;'>When we create a script, whether inline in a script element or included in the web page, We can immediately begin using the API for the document or window objects to manipulate the document itself, or any of the various elements in the web page. </p>
<p style='text-align: justify;'>Our DOM programming may be something as simple as the following example, which displays a message on the console by using the console.log() function: </p>

```html
<body onload="console.log('Welcome to my home page!');">
```
<p style='text-align: justify;'>As it is generally not recommended to mix the structure of the page (written in HTML) and manipulation of the DOM , the JavaScript parts will be grouped together here, and separated from the HTML.</p>

For example, the following function creates a new \<h1 \> element, adds text to that element, and then adds it to the tree for the document:

```html
<html>
  <head>
    <script>
           window.onload = function() {        
         const heading = document.createElement("h1");
         const heading_text = document.createTextNode("Big Head!");
         heading.appendChild(heading_text);
         document.body.appendChild(heading);
      }
    </script>
  </head>
  <body>
  </body>
</html>
```

---


## [Js DOM helper mathods](https://jimfrenette.com/javascript/document/)
1. elementIndex
2. indexInParent
3. indexOfParent
4. matches
5. closest
6. offset top
7. next
8. prev
9. siblings

### 1.elementIndex
Finding the index of element

``` javascript
function elementIndex(el) {
    var index = 0;
    while ((el = el.previousElementSibling)) {
        index++;
    }
    return index;
}
```

### 2.indexInParent

```javascript
function indexInParent(el) {
    let children = el.parentNode.childNodes;
    let num = 0;

    for (let i = 0; i < children.length; i++) {
        if (children[i] == el) return num;
        if (children[i].nodeType == 1) num++;
    }
    return -1;
}
```

### 3.indexOfParent

```javascript
function indexOfParent(el) {
    return [].indexOf.call(el.parentElement.children, el);
}
```

### 4.matches

```javaScript
function matches(elem, selector) {
    const isMsMatch = 'msMatchesSelector' in elem && elem.msMatchesSelector(selector);
    const isMatchSelector = 'matchesSelector' in elem && elem.matchesSelector(selector)
    const isMatch = 'matches' in elem && elem.matches(selector);
  
    return isMsMatch || isMatchSelector || isMatch;
}
```

### 5.closest
`For each element in the set, Get the first element that matches the selector by testing the element itself and traversing up through its ancestors in the DOM tree.`

``` javascript
function getClosest(elem, selector) {

for (elem && elem !== document; elem = elem.parentNode) {       
    if (matches(elem, selector)) {
          
            return elem;
        }
    }
    return null;
}
```

### 6.offset top

```javascript
function getOffsetTop(el) {
    let offsetTop = 0;
    do {
        if (!isNaN(el.offsetTop)) {
            offsetTop += el.offsetTop;
        }
    } while (el = el.offsetParent);
    return offsetTop;
}
```

### 7.next
`Get the immediately following sibling of each element in the set of matched elements.
Depends on matches, prev.`

```javascript
function next(elem, selector) {
    if (elem.nextElementSibling) {
        if (matches(elem.nextElementSibling, selector)) {
            return elem.nextElementSibling;
        } else {
            return prev(elem.nextElementSibling, selector);
        }
    }

    return false;
} 

```

### 8.prev
`Get the immediately preceding sibling of each element in the set of matched elements.
Depends on matches.`

```javascript
function prev(elem, selector) {
    if (elem.previousElementSibling) {
        if (matches(elem.previousElementSibling, selector)) {
            return elem.previousElementSibling;
        } else {
            return prev(elem.previousElementSibling, selector);
        }
    }

    return false;
}
```

### 9.siblings
`Get the siblings of each element in the set of matched elements.
Depends on matches.`

```javascript
function siblings(elem, selector) {
    return Array.prototype.filter.call(elem.parentNode.children, function (child) {
        return matches(child, selector);
    }) || [];
}
```
---

### References

1.DOM -->

https://developer.mozilla.org/en-US/docs/Web/API/Document_Object_Model/Introduction


2.Js DOM helper mathods ->

https://jimfrenette.com/javascript/document/

---



